import { Component, OnInit } from '@angular/core';
import { DeviceService } from '../device.service';
import { Router } from '@angular/router';
import { DeviceTypeService } from '../device-type.service';
import { DeviceType } from '../deviceType';
import { Device } from '../device';

@Component({
  selector: 'app-add-device',
  templateUrl: './add-device.component.html',
  styleUrls: ['./add-device.component.css']
})
export class AddDeviceComponent implements OnInit {

  tempDevice= {};
  deviceTypes: DeviceType[]

  constructor( private deviceService: DeviceService,
               private router: Router,
               private deviceTypeService: DeviceTypeService) { }

  ngOnInit() {
    this.getAllDeviceTypes()
  }

  getAllDeviceTypes(): void {
    this.deviceTypeService.getAll()
    .subscribe(
      res=> {
        console.log(res)
        this.deviceTypes = res
      }
    )
  }

  addDevice(): void {
    this.tempDevice
     console.log(this.tempDevice)
     this.deviceService.addDevice(this.tempDevice)
       .subscribe(
         res => {
           console.log(res)
           this.router.navigate([`/detail/${res.deviceID}`])
         },
         err => alert(err.error)
         )
  }

}
