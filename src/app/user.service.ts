import { Injectable } from '@angular/core';
import {  HttpHeaders, HttpClient } from '@angular/common/http';
import { User } from './user';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { isNgTemplate } from '@angular/compiler';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class UserService {
  

  private loginUrl = 'http://localhost:8080/user/login';
  private verifyUrl = 'http://localhost:8080/user/me';
  private registerUrl = 'http://localhost:8080/user/add'


  login(user) {
    return this.http.post<any>(this.loginUrl, user)
  }

  loggedIn() {
    return !!localStorage.getItem('Token')
  }

  getToken() {
    return localStorage.getItem('Token')
  }

  logoutUser() {
    localStorage.removeItem('userId')
    localStorage.removeItem('Token')
    localStorage.removeItem('roles')
  }

  verify() {
    this.http.get<any>(this.verifyUrl).subscribe(
      res => {
        console.log(res)
        localStorage.setItem('userId', res.userId)
        localStorage.setItem('roles', res.roles)
        return res.userId
      },
      err => console.log(err.error)
      )
  }

  isAdmin() {
    if (this.loggedIn()) {
      return(localStorage.getItem('roles').startsWith("ROLE_ADMIN"))
    }
    else  {
      return false
    }
  }

  register(user) {
    user.admin = false
    return this.http.post<any>(this.registerUrl, user)
    
  }


  constructor(
    private http: HttpClient,
  ) { }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
 
    console.error(error); // log to console instead
 
    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}