import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Device } from './device';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { UserService } from './user.service';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class DeviceService {

  private deviceUrl = 'http://localhost:8080/device';

  getDevice(deviceId: number): Observable<Device> {
    const url = `${this.deviceUrl}/${deviceId}`;
    return this.http.get<Device>(url).pipe(
      tap(_ => console.log(`fetched device id=${deviceId}`)),
      catchError(this.handleError<Device>(`getDevice id=${deviceId}`))
    );
    
  }

  addDevice(device) {
    device.checkedOut = false;
    const url = `${this.deviceUrl}/add`;
    console.log(device)
    return this.http.post<any>(url,device)
  }

  checkOutDevice(device): Observable<Device> {
    this.userService.verify() 
    const url = `${this.deviceUrl}/update`;
    device.checkedOut = true;
    device.checkedOutDate = new Date();
    device.userID = localStorage.getItem('userId')
    return this.http.post<any>(url, device)
  }

  checkInDevice(device): Observable<Device> {
    const url = `${this.deviceUrl}/update`;
    device.checkedOut = false;
    device.userID = null;
    device.checkedOutDate = null;
    return this.http.post<any>(url, device)
  }

  deleteDevice(deviceId: number) {
    const url = `${this.deviceUrl}/${deviceId}`
    return this.http.delete<any>(url)
  }


  constructor(
    private http: HttpClient,
    private userService: UserService
  ) { }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
 
    console.error(error); // log to console instead
 
    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}
