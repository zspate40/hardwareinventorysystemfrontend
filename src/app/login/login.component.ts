import { Component, OnInit, Input } from '@angular/core';
import { User } from '../user';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../user.service';
import { Location } from '@angular/common';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginUser = {};
  user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService,
    private location: Location,
    private router: Router

  ) { }

  ngOnInit() {
    
  }

  login(): void {
    console.log(this.loginUser)
    this.userService.login(this.loginUser)
      .subscribe(
        res => {
          console.log(res)
          localStorage.setItem('userId', res.userId)
          localStorage.setItem('Token', res.token)
          this.userService.verify();
          this.router.navigate(['/search'])
        },
        err => alert(err.error)
        )
  }

  goBack(): void {
    this.location.back()
  }
}
