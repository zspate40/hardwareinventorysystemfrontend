import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DeviceLookupComponent } from './device-lookup/device-lookup.component';
import { DeviceDetailsComponent } from './device-details/device-details.component';
import { LoginComponent } from './login/login.component';
import { AddDeviceComponent } from './add-device/add-device.component';
import { AuthGuard } from './auth.guard';
import { AddDeviceTypeComponent } from './add-device-type/add-device-type.component';
import { RegisterComponent } from './register/register.component';

const routes: Routes = [
  {path: '', redirectTo: '/search', pathMatch: 'full'},
  {path: 'search', component: DeviceLookupComponent},
  {path: 'detail/:id', component: DeviceDetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'addDevice', component: AddDeviceComponent, canActivate: [AuthGuard]},
  {path: 'addDeviceType', component: AddDeviceTypeComponent, canActivate: [AuthGuard]},
  {path: 'register', component: RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
