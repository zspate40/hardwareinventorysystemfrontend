export class User {
    userId: number;
    firstName: String;
    lastName: String;
    admin: Boolean;

}