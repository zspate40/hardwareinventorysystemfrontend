import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  tempUser= {}
  constructor( private userService: UserService,
               private router: Router) { }

  ngOnInit() {
  }

  register(){
    this.userService.register(this.tempUser).subscribe(
      res => {
        console.log(res)
        this.router.navigate(['/login'])
      },
    err => {
      console.log(err)
      alert(err.error)
    }
    )
  }

}
