import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { DeviceType } from './deviceType';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class DeviceTypeService {

  private deviceTypeUrl = 'http://localhost:8080/deviceType'

  getAll() {
    return this.http.get<any>(this.deviceTypeUrl)
  }

  getDeviceType(id: number) {
    return this.http.get<any>(`${this.deviceTypeUrl}/${id}`)
  }

  addDeviceType(deviceType) {
    return this.http.post<any>(`${this.deviceTypeUrl}/add`, deviceType)
  }

  constructor(
    private http: HttpClient
  ) { }

    /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
 
    console.error(error); // log to console instead
 
    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
  }
}
