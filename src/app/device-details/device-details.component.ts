import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Device } from '../device';
import { DeviceService } from '../device.service';
import { Location } from '@angular/common';
import { UserService } from '../user.service';
import { DeviceTypeService } from '../device-type.service';
import { DeviceType } from '../deviceType';

@Component({
  selector: 'app-device-details',
  templateUrl: './device-details.component.html',
  styleUrls: ['./device-details.component.css']
})
export class DeviceDetailsComponent implements OnInit {

  device: Device;
  deviceType= {};
  userId = localStorage.getItem('userId')
  

  constructor(
    private route: ActivatedRoute,
    private deviceService: DeviceService,
    private location: Location,
    private userService: UserService,
    private deviceTypeService: DeviceTypeService,
    private router: Router

  ) { }

  ngOnInit() {
    this.getDevice()
    this.userService.verify()
    
  }

  getDevice(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.deviceService.getDevice(id)
      .subscribe(device => {
        this.device = device;
        this.deviceTypeService.getDeviceType(this.device.deviceType)
          .subscribe(deviceType => this.deviceType = deviceType)
            
    }
      )
      //console.log(this.deviceType)
      //console.log(this.deviceType.deviceTypeName)
      //this.getDeviceType()
  }

  getDeviceType(): void {
    this.deviceTypeService.getDeviceType(this.device.deviceType)
    .subscribe(
      res => {
        console.log(res)
      },
      err => alert(err.error)
    )
  }

  checkOutDevice(): void {
    this.deviceService.checkOutDevice(this.device)
    .subscribe(device => {
      this.device = device
      if (this.device.userID != +localStorage.getItem('userId')){
        this.deviceService.checkOutDevice(this.device)
        .subscribe(device => this.device = device)
      }} 
      );
    //console.log(this.device.checkedOutDate)
  }

  deleteDevice(): void {
    this.deviceService.deleteDevice(this.device.deviceID)
    .subscribe(res => {
      alert("Device Deleted")
      this.router.navigate(['/search'])
    },
    err =>
    console.log(err.error))
  }

  checkInDevice(): void {
    this.deviceService.checkInDevice(this.device)
    .subscribe(device => this.device = device);
    //console.log(this.device.checkedOutDate)
  }


  goBack(): void {
    this.location.back();
  }
}
