import { Injectable } from '@angular/core';
import { CanActivate ,Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { UserService } from './user.service';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
 
  constructor(private userService: UserService,
              private router: Router) {}

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if(!!localStorage.getItem('roles')){
      if (localStorage.getItem('roles').startsWith("ROLE_ADMIN")){
        return true
      } else {
        alert("Only admins can perform selected task")
        this.router.navigate(['/search'])
        return false
      }
    } else{
      alert("Only admins can perform selected task")
        this.router.navigate(['/search'])
        return false
    }
  }      
  
}
