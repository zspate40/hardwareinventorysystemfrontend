import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MaterialModule } from './material';
import { MatIconModule } from '@angular/material';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { DeviceLookupComponent } from './device-lookup/device-lookup.component';
import { DeviceDetailsComponent } from './device-details/device-details.component';
import { HttpClientModule, HTTP_INTERCEPTORS }    from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import { LoginComponent } from './login/login.component';
import {MatInputModule} from '@angular/material/input';
import { TokenInterceptorService } from './token-interceptor.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { AddDeviceComponent } from './add-device/add-device.component';
import { AuthGuard } from './auth.guard';
import {MatSelectModule} from '@angular/material/select';
import { AddDeviceTypeComponent } from './add-device-type/add-device-type.component';
import { RegisterComponent } from './register/register.component';
import {MatCardModule} from '@angular/material/card';



@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    DeviceLookupComponent,
    DeviceDetailsComponent,
    LoginComponent,
    AddDeviceComponent,
    AddDeviceTypeComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatIconModule,
    HttpClientModule,
    FormsModule,
    MatMenuModule,
    MatInputModule,
    MatSnackBarModule,
    MatSelectModule,
    MatCardModule
  ],
  providers: [AuthGuard, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
