import { Injectable, Injector } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http'
import { UserService } from './user.service';
import { nextContext } from '@angular/core/src/render3';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor{

  constructor(private injector: Injector) { }

  intercept(req, next){
    let userService = this.injector.get(UserService)
    console.log(req)
    if (userService.loggedIn()){
    let tokenizeReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${userService.getToken()}`
      }
    })
    console.log(tokenizeReq)
    return next.handle(tokenizeReq)
  } else {
    return next.handle(req)
  }
  }
}
