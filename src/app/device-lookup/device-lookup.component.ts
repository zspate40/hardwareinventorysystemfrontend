import { Component, OnInit, Input } from '@angular/core';
import { DeviceService } from '../device.service';
import { Device } from '../device';

@Component({
  selector: 'app-device-lookup',
  templateUrl: './device-lookup.component.html',
  styleUrls: ['./device-lookup.component.css']
})
export class DeviceLookupComponent implements OnInit {

  @Input() deviceId: number;

  constructor(
    private deviceService: DeviceService
  ) { }

  ngOnInit() {
  }

  
}
