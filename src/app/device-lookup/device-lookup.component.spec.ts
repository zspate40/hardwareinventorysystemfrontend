import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceLookupComponent } from './device-lookup.component';

describe('DeviceLookupComponent', () => {
  let component: DeviceLookupComponent;
  let fixture: ComponentFixture<DeviceLookupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceLookupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceLookupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
