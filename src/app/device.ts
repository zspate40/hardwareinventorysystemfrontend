export class Device {
    deviceID: number;
    deviceType: number;
    userID: number;
    checkedOut: boolean;
    checkedOutDate: Date;
}