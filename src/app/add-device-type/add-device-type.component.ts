import { Component, OnInit } from '@angular/core';
import { DeviceTypeService } from '../device-type.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-device-type',
  templateUrl: './add-device-type.component.html',
  styleUrls: ['./add-device-type.component.css']
})
export class AddDeviceTypeComponent implements OnInit {
  
  tempDeviceType= {};
  constructor( private deviceTypeService: DeviceTypeService,
               private router: Router) { }

  ngOnInit() {
  }

  addDeviceType(): void {
    this.tempDeviceType
     console.log(this.tempDeviceType)
     this.deviceTypeService.addDeviceType(this.tempDeviceType)
       .subscribe(
         res => {
           console.log(res)
           this.router.navigate([`/detail/${res.deviceID}`])
         },
         err => alert(err.error)
         )
  }

}
